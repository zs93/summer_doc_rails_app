class CreateMovies < ActiveRecord::Migration[6.0]
  def change
    create_table :movies do |t|
      t.integer :movieId
      t.string :title
      t.string :genres

      t.timestamps
    end
  end
end
