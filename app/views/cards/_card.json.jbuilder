json.extract! card, :id, :suit, :color, :value, :created_at, :updated_at
json.url card_url(card, format: :json)
