class Rating < ApplicationRecord
    belongs_to :movie, foreign_key: "movieId"
end
